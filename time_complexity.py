import merge_sort

result_list= merge_sort.merge_sort([5, 3, 4, 1, 9, 6, 2])
print(result_list)


# Define a funtion to measure running time
def runningtime(sample):
    start = time.time()
    merge_sort(sample)
    return time.time() - start


# Main
import time
import random

i = 0
elements = [1000, 2000, 4000, 8000, 16000, 32000]
Best_result = list()
Worst_result = list()
Avg_result = list()

print("elements\t\tBest case\t\tWorst case\t\tAverage Case")
for num in elements:
    Best = list(range(1, num + 1))
    Best_result.append(runningtime(Best))
    Worst = list(range(num, 0, -1))
    Worst_result.append(runningtime(Worst))
    Avg = list(range(1, num + 1))
    random.shuffle(Avg)
    Avg_result.append(runningtime(Avg))
    print("{}\t\t\t{:.3f}\t\t\t{:.3f}\t\t\t{:.3f}".format(elements[i], Best_result[i], Worst_result[i], Avg_result[i]))
    i += 1

# Show plots
import matplotlib.pyplot as plt


plt.plot(elements, Best_result, label = "Best Case")
plt.plot(elements, Worst_result, label = "Worst Case")
plt.plot(elements, Avg_result, label = "Avg Case")
plt.legend()
plt.show()